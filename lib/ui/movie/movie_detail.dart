import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MovieDetail extends StatelessWidget {
  final data;
  const MovieDetail({Key? key, required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateTime tempDate =
        new DateFormat("yyyy").parse(data.releaseDate ?? data.firstAirDate);
    Widget imageAndTittle() {
      return Container(
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 15),
              width: double.infinity,
              height: 300,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(0),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  colorFilter: new ColorFilter.mode(
                      Colors.black.withOpacity(0.7), BlendMode.dstATop),
                  image: NetworkImage(
                    "https://image.tmdb.org/t/p/w500/" + data.posterPath!,
                  ),
                ),
              ),
              child: Align(
                alignment: Alignment.topRight,
                child: Container(
                  width: 55,
                  height: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(18),
                    color: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        width: 20,
                        height: 20,
                        margin: EdgeInsets.only(right: 2),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage('assets/icon_star.png'),
                          ),
                        ),
                      ),
                      Text(
                        '${data.voteAverage.toString()}',
                        style: TextStyle(
                            fontSize: 14, fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ),
              ),
              // child: Image.network(
              //     "https://image.tmdb.org/t/p/w500/" + data.posterPath!),
            ),
            Container(
              margin: EdgeInsets.only(top: 230, left: 25),
              child: Row(
                children: [
                  Container(
                    width: 150,
                    height: 150,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(18),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                          "https://image.tmdb.org/t/p/w500/" + data.posterPath!,
                        ),
                      ),
                    ),

                    // child: Image.network(
                    //     "https://image.tmdb.org/t/p/w500/" + data.posterPath!),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(top: 70),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${data.title ?? data.name} (${tempDate.year})',
                            style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                            ),
                            overflow: TextOverflow.clip,
                          ),
                          SizedBox(
                            height: 2,
                          ),
                          Text(
                            'Popularity : ${data.popularity.toString()}',
                            style: TextStyle(
                              color: Colors.grey,
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    }

    Widget overviewAndPopularity() {
      return Container(
        margin: EdgeInsets.only(top: 15, left: 25, right: 25),
        padding: EdgeInsets.all(8),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(18),
          color: Colors.grey[100],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Overview',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              data.overview,
              textAlign: TextAlign.justify,
            ),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: ListView(
        children: [
          imageAndTittle(),
          overviewAndPopularity(),
        ],
      ),
    );
  }
}
