import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/DatabaseHandler/DbHelper.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/common/widget/custom_button.dart';
import 'package:majootestcase/common/widget/text_form_field.dart';
import 'package:majootestcase/main.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/loading.dart';
import 'package:majootestcase/ui/home/home_bloc_screen.dart';
import 'package:majootestcase/ui/home/home_page_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextController(initialValue: '');
  final _nameController = TextController(initialValue: '');
  final _passwordController = TextController(initialValue: '');
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Register User',
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  // color: colorBlue,
                ),
              ),
              Text(
                'Silahkan register terlebih dahulu',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SizedBox(
                height: 9,
              ),
              _form(),
              SizedBox(
                height: 50,
              ),
              BlocConsumer<AuthBlocCubit, AuthBlocState>(
                listener: (context, state) {
                  if (state is AuthBlocLoginState) {
                    Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                        builder: (_) => BlocProvider(
                          create: (context) =>
                              AuthBlocCubit()..fetch_history_login(),
                          child: MyHomePageScreen(),
                        ),
                      ),
                      (route) => false,
                    );
                  } else if (state is AuthBlocErrorState) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(
                        backgroundColor: Colors.red,
                        content: Text(state.error),
                      ),
                    );
                  }
                },
                builder: (context, state) {
                  if (state is AuthBlocLoadingState) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                  return CustomButton(
                    text: 'Register',
                    onPressed: handleRegist,
                    height: 100,
                  );
                },
              ),
              SizedBox(
                height: 50,
              ),
              _register(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
            },
          ),
          CustomTextFormField(
            context: context,
            controller: _nameController,
            isEmail: false,
            hint: 'Your Name',
            label: 'Name',
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _register() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (_) => BlocProvider(
                  create: (context) => AuthBlocCubit()..fetch_history_login(),
                  child: MyHomePageScreen(),
                ),
              ),
              (route) => true);
        },
        child: RichText(
          text: TextSpan(
              text: 'Sudah punya akun? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Silahkan Login',
                ),
              ]),
        ),
      ),
    );
  }

  void handleRegist() async {
    final _email = _emailController.value;
    final _name = _nameController.value;
    final _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _name != null &&
        _password != null) {
      // AuthBlocCubit authBlocCubit = AuthBlocCubit();
      User user = User(
        email: _email,
        userName: _name,
        password: _password,
      );
      // authBlocCubit.register_user(user);
      context.read<AuthBlocCubit>().register_user(user);
    }
  }
}
