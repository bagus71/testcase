import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/DatabaseHandler/DbHelper.dart';
import 'package:majootestcase/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetch_history_login() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void login_user(User user) async {
    try {
      var dbHelper = DbHelper();
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      emit(AuthBlocLoadingState());

      // String data = user.toJson().toString();
      var dataCall = (await dbHelper.userLogin(user.email!, user.password!));
      String data = dataCall!.toJson().toString();

      sharedPreferences.setString("user_value", data);
      await sharedPreferences.setBool("is_logged_in", true);

      emit(AuthBlocLoggedInState());
    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }

  void register_user(User user) async {
    try {
      var dbHelper = DbHelper();
      emit(AuthBlocLoadingState());
      await dbHelper.saveData(user);
      emit(AuthBlocLoginState());
    } catch (e) {
      emit(AuthBlocErrorState(e.toString()));
    }
  }
}
