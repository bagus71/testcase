import 'dart:ffi';

import 'package:majootestcase/models/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'dart:io' as io;

class DbHelper {
  static Database? _db;

  static const String DB_Name = 'user.db';

  static const String Table_User = 'user';

  static const int Version = 1;

  static const String User_ID = 'id';
  static const String User_Email = 'email';
  static const String User_Name = 'username';
  static const String User_Password = 'password';

  Future<Database> get db async {
    if (_db != null) {
      return _db!;
    }

    _db = await initDb();

    return _db!;
  }

  initDb() async {
    io.Directory documentsDirectory = await getApplicationDocumentsDirectory();

    String path = join(documentsDirectory.path, DB_Name);

    var db = await openDatabase(path, version: Version, onCreate: _onCreate);
    return db;
  }

  _onCreate(Database db, int intVersion) async {
    await db.execute('CREATE TABLE $Table_User ('
        ' $User_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, '
        ' $User_Email TEXT, '
        ' $User_Name TEXT,'
        ' $User_Password TEXT'
        ')');
  }

  Future<void> saveData(User user) async {
    try {
      var dbClient = await db;
      await dbClient.insert(Table_User, user.toJson());
      // user = () as User;
      // return user;
    } catch (e) {
      throw e;
    }
  }

  Future<User?> userLogin(String email, String password) async {
    try {
      var dbClient = await db;
      var res = await dbClient.rawQuery("SELECT * FROM $Table_User WHERE "
          "$User_Email = '$email' AND "
          "$User_Password = '$password'");
      if (res.length > 0) {
        return User.fromJson(res.first);
      } else {
        throw 'User dengan email yang ada input tidak ada ';
      }
    } catch (e) {
      throw e;
    }
  }
}
