class User {
  int? id;
  String? email;
  String? userName;
  String? password;

  User({
    this.id,
    this.email,
    this.userName,
    this.password,
  });

  User.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{
      'email': email,
      'password': password,
      'username': userName
    };

    if (id != null) {
      map['id'] = id;
    }

    return map;
  }
}
